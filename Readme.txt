Procedure:


1. Clone or download the repository on your workstation

2. Build the project using Maven: $ mvn package
3. From the TARGET folder, copy the "ixi-zeromq-jar-with-dependencies" file to your ICT node (they have to be installed on the same VPS/workstation/PC)
4. Activate the JAR setting two parameters, in this way:
	$ java -jar ixi-zeromq-jar-with-dependencies.jar {ICT_NAME} {ZMQ_PORT}
5. {ICT_NAME} is the name of your ICT node
6. {ZMQ_PORT} is the TCP port where ZMQ data stream will be flowing; use 5560 as default port
7. Use a ZMQ listener to collect the realtime data; this is a listener in NodeJS: https://gitlab.com/Stefano_Core/iota-ict-zmq-listener
8. 
Press CTRL+C to stop the service

Available listeners:
- NodeJS: https://gitlab.com/Stefano_Core/iota-ict-zmq-listener
- Python: https://gitlab.com/Stefano_Core/iota-ict-zmq-listener-py