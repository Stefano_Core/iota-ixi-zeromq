import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Running IXI module for ICT version 0.3");

        String ictName = args.length >= 2 ? args[0] : "";
        String zmqPort = args.length >= 2 ? args[1] : "";

        if(args.length < 2) {
            System.err.println("WARNING: No arguments were passed to IXI module.");
            System.out.println("You can start ixi-zeromq like this:    java -jar ixi-zeromq-{VERSION}.jar {ICT_NAME} {ZMQ_PORT}");
            System.out.println();

            Scanner scanner = new Scanner(System.in);

            System.out.println("Please enter the name of your ICT: ");
            System.out.print("> ");
            ictName = scanner.nextLine();
            System.out.println();

            System.out.println("Please enter the TCP port you want to use to access ZMQ data (Default: 5560): ");
            System.out.print("> ");
            zmqPort = scanner.nextLine();
            System.out.println();
        }

        try {
            new zmqListenerIxi(ictName, zmqPort);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}






