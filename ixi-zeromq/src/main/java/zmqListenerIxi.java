import org.iota.ict.ixi.IxiModule;
import org.iota.ict.network.event.GossipFilter;
import org.iota.ict.network.event.GossipReceiveEvent;
import org.iota.ict.network.event.GossipSubmitEvent;
import org.zeromq.ZMQ;

public class zmqListenerIxi extends IxiModule {
    public static final String NAME = "zmq-listener.ixi";

    //ZMQ server setup
    ZMQ.Context context = ZMQ.context(1);
    ZMQ.Socket publisher = context.socket(ZMQ.PUB);

    public zmqListenerIxi(String ictName, String zmqPort) {
        super("zmq-listener.ixi", ictName);
        System.out.println("ixi-zeromq started...");
        this.setGossipFilter((new GossipFilter()).setWatchingAll(true));
        System.out.println("Activating ZMQ server...");
        publisher.bind("tcp://*:" + zmqPort);
        System.out.println("Start publishing data...");
    }

    public void onTransactionReceived(GossipReceiveEvent event) {
        System.out.println("New TX coming....");
        System.out.println(event.getTransaction().hash);
        System.out.println(event.getTransaction().decodedSignatureFragments);
        System.out.println();
        StringBuilder dataToSend = new StringBuilder(1000);

        try {
            dataToSend.append("tx ");
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().hash);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().signatureFragments);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().address);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().trytes);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().isBundleHead);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().timelockLowerBound);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().timelockUpperBound);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().attachmentTimestampLowerBound);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().attachmentTimestamp);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().attachmentTimestampUpperBound);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().branchHash);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().trunkHash);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().essence);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().extraDataDigest);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().nonce);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().tag);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().value);
            dataToSend.append("|");
            dataToSend.append(event.getTransaction().decodedSignatureFragments);

            publisher.send(dataToSend.toString());

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void onTransactionSubmitted(GossipSubmitEvent event) {
        System.out.println("Submitted message: " + event.getTransaction().decodedSignatureFragments);
    }
}